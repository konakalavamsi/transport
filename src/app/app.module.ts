import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserdashboardComponent } from './user/userdashboard/userdashboard.component';
import { NavbarComponent } from './user/navbar/navbar.component';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateworkitemsComponent } from './user/createworkitems/createworkitems.component';

@NgModule({
  declarations: [
    AppComponent,
    UserdashboardComponent,
    NavbarComponent,
    CreateworkitemsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [CreateworkitemsComponent]
})
export class AppModule { }
