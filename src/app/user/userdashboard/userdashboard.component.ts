import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserdashboardService } from 'src/app/userservice/userdashboard/userdashboard.service';
import {LiveAnnouncer} from '@angular/cdk/a11y';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CreateworkitemsComponent } from '../createworkitems/createworkitems.component';

@Component({
  selector: 'app-userdashboard',
  templateUrl: './userdashboard.component.html',
  styleUrls: ['./userdashboard.component.css']
})
export class UserdashboardComponent implements OnInit,AfterViewInit {
  displayedColumns:String[]=['itemName','itemType','capacity','collectionCountry',"destinationCountry",'HarborLocation','shippingDate','Amount','actions']
 
  constructor(private service: UserdashboardService,private _liveAnnouncer: LiveAnnouncer,private dialog:MatDialog) { }
  
  workitems!: MatTableDataSource<any>;

  
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('paginator') paginator!: MatPaginator;

  searchKey: any;
  ngOnInit(): void {
  this.service.getworkitems().subscribe(
      (result)=>{
       
        this.workitems = new MatTableDataSource(result);
        
        
      }
    )



  }
  ngAfterViewInit(): void {

    this.workitems.sort=this.sort
    this.workitems.paginator=this.paginator;
  }
  announceSortChange(sortState: Sort) {
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
  onsearchclear(){
    this.searchKey="";
    this.applyFilter();
  }
  applyFilter(){
    this.workitems.filter=this.searchKey.trim().toLowerCase();
  }
  onCreate(){
    this.service.initializeFormGroup();
      const dialogConfig=new MatDialogConfig();
      dialogConfig.disableClose=true;
      dialogConfig.autoFocus=true;
      dialogConfig.width="60%";
      this.dialog.open(CreateworkitemsComponent,dialogConfig);
  }
  onEdit(row: any){
      this.service.populateForm(row);
      const dialogConfig=new MatDialogConfig();
      dialogConfig.disableClose=true;
      dialogConfig.autoFocus=true;
      dialogConfig.width="60%";
      this.dialog.open(CreateworkitemsComponent,dialogConfig);

  }

}
