import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UserdashboardService } from 'src/app/userservice/userdashboard/userdashboard.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-createworkitems',
  templateUrl: './createworkitems.component.html',
  styleUrls: ['./createworkitems.component.css']
})
export class CreateworkitemsComponent implements OnInit {

  constructor(private service: UserdashboardService,
    private dialogRef:MatDialogRef<CreateworkitemsComponent>) {
    this.form = this.service.form;
   }
  countries:any=[
    {"name": "singapore", "id": 1}, 
    {"name": "Honduras", "id": 2}, 
    {"name": "Hong Kong", "id": 3}, 
    {"name": "Hungary", "id": 4}, 
    {"name": "Iceland", "id": 5}, 
    {"name": "india", "id": 6}, 
    {"name": "indonesia", "id": 7}, 
    {"name": "Iraq", "id": 8}, 
    {"name": "Ireland", "id": 9},
    {"name": "Israel", "id": 10}, 
    {"name": "Italy", "id": 11}, 
    {"name": "Jamaica", "id": 12}, 
    {"name": "Japan", "id": 13}, 
    {"name":"usa","id":14}
  
  ]
  form!: FormGroup;
 
  ngOnInit(): void {
    
    
  }
  submitForm(){
    if(this.form.valid){
      console.log(this.form.value.workitemId)
        if(this.form.value.workitemId==null)
            this.service.saveworkitem(this.form.value)
        else
          this.service.updateworkitem(this.form.value)
        this.form.reset();
        this.service.initializeFormGroup();
        this.form.markAsUntouched();
          Object.keys(this.form.controls).forEach((name) => {
            var control = this.form.controls[name];
            control.setErrors(null);
          });
        this.onClose();
    
    }
  }
  
  errors:any;
  myError = (controlName: string, errorName: string) =>{
    
    this.errors=this.form.controls[controlName].errors
    if(this.errors==null){
      return false
    }
    
    return this.errors[errorName]
    }
    onClear(){

      
      this.service.initializeFormGroup();
      
      Object.keys(this.form.controls).forEach((name) => {
        var control = this.form.controls[name];
        control.setErrors(null);
      });

    }
    onClose(){
      this.form.reset();
      this.service.initializeFormGroup();
      this.dialogRef.close();
    }
    
}
