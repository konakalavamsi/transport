import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateworkitemsComponent } from './createworkitems.component';

describe('CreateworkitemsComponent', () => {
  let component: CreateworkitemsComponent;
  let fixture: ComponentFixture<CreateworkitemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateworkitemsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateworkitemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
