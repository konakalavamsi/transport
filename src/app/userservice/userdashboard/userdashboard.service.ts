import { Injectable } from '@angular/core';
import { FormControl, FormGroup ,Validators} from '@angular/forms';
import * as _ from 'lodash';

import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserdashboardService {
  
  constructor() { }
  workitems:any=[
    {workitemId:'1234',itemName:'washing machine',itemType:'electronice',capacity:"4500",itemDescription:'something',messageToRecipient:'something',sourceCountry:"usa",destinationCountry:'india',availableHarborLocations:'indonesia',shippingDate:"Mon Jul 25 2022",Amount:'1000'},
    {workitemId:'1235',itemName:'washing machine',itemType:'electronice',capacity:"4500",collectionCountry:"usa",destinationCountry:'india',HarborLocation:'Tanjong Pager',shippingDate:"12-Aug-20",Amount:'1000'},
    {workitemId:'1236',itemName:'washing machine',itemType:'electronice',capacity:"4500",collectionCountry:"usa",destinationCountry:'india',HarborLocation:'Tanjong Pager',shippingDate:"12-Aug-20",Amount:'1000'},
    {workitemId:'1237',itemName:'washing machine',itemType:'electronice',capacity:"4500",collectionCountry:"usa",destinationCountry:'india',HarborLocation:'Tanjong Pager',shippingDate:"12-Aug-20",Amount:'1000'},
    {workitemId:'1238',itemName:'washing machine',itemType:'electronice',capacity:"4500",collectionCountry:"usa",destinationCountry:'india',HarborLocation:'Tanjong Pager',shippingDate:"12-Aug-20",Amount:'1000'},
    {workitemId:'1239',itemName:'washing machine',itemType:'electronice',capacity:"4500",collectionCountry:"usa",destinationCountry:'india',HarborLocation:'Tanjong Pager',shippingDate:"12-Aug-20",Amount:'1000'},
    {workitemId:'1240',itemName:'washing machine',itemType:'electronice',capacity:"4500",collectionCountry:"usa",destinationCountry:'india',HarborLocation:'Tanjong Pager',shippingDate:"12-Aug-20",Amount:'1000'},
    {workitemId:'1241',itemName:'washing machine',itemType:'electronice',capacity:"4500",collectionCountry:"usa",destinationCountry:'india',HarborLocation:'Tanjong Pager',shippingDate:"12-Aug-20",Amount:'1000'},
    {workitemId:'1242',itemName:'awashing machine',itemType:'electronice',capacity:"4500",collectionCountry:"usa",destinationCountry:'india',HarborLocation:'Tanjong Pager',shippingDate:"12-Aug-20",Amount:'1000'},
    {workitemId:'1243',itemName:'bwashing machine',itemType:'electronice',capacity:"5000",collectionCountry:"usa",destinationCountry:'india',HarborLocation:'Tanjong Pager',shippingDate:"12-Aug-20",Amount:'500'}
  ]
  getworkitems(){
      return of(this.workitems); 
  }
  workitemId(l:any){
      var text = "";
      var char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for(var i=0; i < l; i++ )
      {  
      text += char_list.charAt(Math.floor(Math.random() * char_list.length));
      }
      return text;
  }
  saveworkitem(workitem: any){
      console.log('inserted');
  }
  updateworkitem(workitem: any){
    console.log("updated")
  }
  form: FormGroup=new FormGroup({
    workitemId:new FormControl(null),
    itemName:new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(25)]),
    itemType:new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(25)]),
    itemDescription:new FormControl('',[Validators.required,Validators.minLength(10),Validators.maxLength(45)]),
    messageToRecipient:new FormControl('',[Validators.required,Validators.minLength(10),Validators.maxLength(50)]),
    capacity:new FormControl('',[Validators.required,Validators.min(1),Validators.pattern("^[0-9]*$")]),
    sourceCountry:new FormControl('',Validators.required),
    destinationCountry:new FormControl('',Validators.required),
    availableHarborLocations:new FormControl('',Validators.required),
    shippingDate:new FormControl('',Validators.required),
  });



  initializeFormGroup(){
    this.form.setValue({
      workitemId:null,
      itemName:'',
      itemType:'',
      itemDescription:'',
      messageToRecipient:'',
      capacity:'',
      sourceCountry:'',
      destinationCountry:'',
      availableHarborLocations:'',
      shippingDate:'',
     
    })
  }
  populateForm(workitem:any){
    this.form.setValue({
      workitemId:workitem.workitemId,
      itemName:workitem.itemName,
      itemType:workitem.itemType,
      itemDescription:workitem.itemDescription,
      messageToRecipient:workitem.messageToRecipient,
      capacity:workitem.capacity,
      sourceCountry:workitem.sourceCountry,
      destinationCountry:workitem.destinationCountry,
      availableHarborLocations:'1',
      shippingDate:workitem.shippingDate,
      
    })
    
  }
}
